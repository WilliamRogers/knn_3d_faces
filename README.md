# README #

### What is this repository for? ###

* Just some quick code to demonstrate a algortithm based on KNN that is used to
* find matching faces of 3D models.  It's done by esentially calculating
* the difference by using the euclidean distance of the nearest points to see how
* far a part they are then simply ranks the most similar faces.  

* Version 0.0.1

### DATA ###
* Five faces were extracted to a .csv from .obj files.  It's in a very simple
* format where each line in the .csv file represent a single point in a 3d
* space, making it very easy to calculate the Euclidean Distance.

### Future Updates ###
* This was just a quick simple project to demonstrate the capability of a KNN
* outside its traditional use.  It does have one major drawback, it doesn't
* account for different sized faces that may actually also be very much similar
* (maybe caused by age differences).  One way to account for this would be to
* resize a face at their optimal similarities, which could again be determined
* using the euclidean distance.

### Who do I talk to? ###

* William Rogers
* wrrogers@hotmail.com