import tqdm
from time import sleep
import pandas as pd
import numpy as np

#This is a basic version of K nearest neighbors
#Normally there would be a large dataset to select K number of matches
#Given a limited number of 3D models this program just prints out the similarity

#The following code requires face1 - face5.csv

def KNearestNeighbors(X, y, k = 3):
    N = X.shape[0]
    euclideans = np.zeros((N, 5169))
    distances  = np.zeros((N, ))

    for n in range(N):
        print ("\nLet's do face",n+1,"...")
        count = 0
        for yVertices in tqdm.tqdm(y):
            sleep(0.01)
            temp = 999
            for XVertices in X[n]:
                if (np.linalg.norm(yVertices - XVertices) < temp):
                    temp = np.linalg.norm(yVertices - XVertices) #Euclidean Distance
            euclideans[n][count] = temp
            count += 1
        distances[n] = np.sum(euclideans[n])
        print("The total distance of", n+1, "was", distances[n])

    count = 0
    for distance in distances:
        count+=1
        print ("The total distance for face", count, "was", distance)

def getFaces(): #Gets the faces that are to be matched
    faces = np.zeros((5,5169,3))
    for f in range(0, 5):
        faces[f] = pd.read_csv('face{}.csv'.format(f+1)).as_matrix()
    return faces

def getTarget():
    return pd.read_csv('face3.csv').as_matrix()

X = getFaces()
y = getTarget()

KNN = KNearestNeighbors(X, y, k=3)